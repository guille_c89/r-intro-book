# 1 Introduction and preliminaries

[[_TOC_]]

## 1.1  The R environment

- `R` is an integrated suite of software facilities such:
  + data handling and storage
  + operations over arrays, specially matrices
  + tools for data analysis
  + generation of plots
  + `S` programming language:
    * conditionals
    * loops
    * recursive functions
    * I/O
- Environment: planned and coherent system (UNIX philosophy?).
- Extended thought _"packages"_.
- Most programs are **ephemeral**, erased at end of session (save to file).

## 1.2 Related software and documentation

- `R`: a `S` language's implementation.
- `S`' evolution is characterized by four books.

## 1.3 `R` and statistics

- About `25` _"standard"_/_"recommended"_ packages.
- Search CRAN Internet sites for more specific/advanced statistics tools.
- `S`(and `R`) philosophy:
  + series of steps with minimal output.
  + intermediate results stored in _objects_ for further processing.

## 1.4 `R` and the window system

- Need of `GUI` for plot visualization.
- No fully portable, some variations by operating system.

## 1.5 Using R interactively

- Prompt: waiting for command input.
- Default prompt is `>`.
- Procedure for a first session:
  1. Create a working directory to hold data files by particular problem.
     ```bash
     $ mkdir poblation_analisys
     $ cd poblation_analisys
     ```
  1. Start `R` (capital R):
     ```bash
     $ R
     ```
  1. Issue commands...
  1. Quit `R` with command `q()`:
     ```R
     > q()
     ```

     >**Note:** `CTRL + D` also works to quit.

     Choose `y` option if you want save data of current session.

## 1.7 Getting help with functions and features

- `help()`: information on named function.
  ```R
  > help(solve)
  ```
- `?`: alternative to `help()`.
  ```R
  > ?solve
  ```
- Special characters (operators?) and keywords, needs double `"` or single `'`.
  ```R
  > help("[[")
  > ?"if"
  ```

  > **Note:** double quote `"` is preferred.

- `help.start()`: help on HTML format.

  > **Note:** command to select a specific web browser (needed on `Arch
  > Linux`): `options(browser="firefox")`

  > **Note:** this launch a local web server.

- `help.search()`: search a term on help.
  ```R
  > help.search(solve)
  ```
- `??`: alternative to `help.search()`. 
  ```R
  > ??solve
  ```

## 1.8 R commands, case sensitivity, etc.

- `R` is an _expression_ language.
- _Case sensitive_: `A` and `a` are different symbols.
- Valid names depends on operating system's _locale_.
- For portability stick to `A-Za-z0-9` (only **ASCII**, no `á` or `ñ`).
- Names can combine alphanumeric (ASCII) symbols, dot `.` and underscore `_`.
- Names must start with letter or dot `.`.
- If name begins with dot `.`, next character **must not** be a digit.
- No limit on name length.
- Elementary commands:
  + Expressions
    * evaluated
    * printed (by default)
    * value is **lost**
  + Assignments
    * evaluates an _expression_
    * value to a variable
    * no printed (by default)
- Commands are separated by:
  - Semi-colon `;`
  - New line (end of line)
- Compound expression:
  + Between braces `{}`:
    - A series of _elementary commands_ (expressions and assignments).
- Comments: begins with hash mark `#` until end of line.
- Comments **not** inside strings or argument list on function definition.
- Different prompt (by default `+`) when command isn't completed at end of line.
- Limit of `4095` bytes by line on console, use script file instead.

## 1.9 Recall and correction of previous commands

- Console on Unix uses `readline` library (vi-mode available).
- Vertical arrow keys scroll over command history.

## 1.10 Executing commands from or diverting output to a file

- `source("file_path.R")`: execute external file with commands.
- `.R`: file extension of `R`'s script.
- `sink("file_path.lis")`: save output of console to a specific file.
- `sink()`: reset output to console.

## 1.11 Data permanency and removing objects

- _Objects_: entities that `R` creates and manipulates:
  + variables
  + arrays of numbers
  + character strings
  + functions
  + structures build from combination of abode.
- _Objects_ are created and stored by name.
- `objects()`: list names of objects.
- `ls()`: alternative to `objects()`.
- _Workspace_: collection of objects currently stored.
- `rm(var1,...,varN)`: remove objects from _workspace_.
- _Workspace_ can be saved to the `.RData` file on working directory.
- Command line history goes to `.Rhistory` file.
- Use separate working directories to avoid namespace collisions.
