digraph "intro_pre" {
// Config
graph [
label = <<b>Introduction and Preliminaries</b>>
labelloc = t
fontsize = 20
layout = dot
rankdir = LR
newrank = true
]

node [
style = filled
shape = rect
pencolor = "#00000044" // frames color
shape = plaintext
]

edge [
arrowsize = 0.5
labeldistance = 3
labelfontcolor = "#00000080"
penwidth = 2
style = dotted
]

// Nodes

// <b><font color="blue" face="Source Code Pro">code</font></b>
// <i><font color="forestgreen">concept</font></i>
// <b><font color="red">IMPORTANT</font></b>
// <br align="left"/>

r_enviro [label=<<b><font color="blue" face="Source Code Pro">R</font></b>
 environment:<br
align="left"/>
- A planned and <b><font color="red">coherent</font></b> system with:<br
align="left"/>
 + data handling and storage<br align="left"/>
 + operations over arrays, specially matrices<br align="left"/>
 + tools for data analysis<br align="left"/>
 + generation of plots<br align="left"/>
 + <b><font color="blue" face="Source Code Pro">S</font></b> programming
 language.<br align="left"/>
- Need of GUI for plot visualization.<br align="left"/>
- <b><font color="red">Not</font></b> fully portable, some variations by
 operating system.<br align="left"/>

>]

s_lang [label=<<b><font color="blue" face="Source Code Pro">S</font></b>
 programming language:<br align="left"/>
- Features:<br align="left"/>
 +  conditionals<br align="left"/>
 +  loops<br align="left"/>
 +  recursive functions<br align="left"/>
 +  I/O<br align="left"/>
- <b><font color="blue" face="Source Code Pro">R</font></b> is an
 implementation.<br align="left"/>
- Evolution is characterized by four books.<br align="left"/>
>]

s_philosophy [label=<Philosophy:<br align="left"/>
-  Series of steps with <b><font color="red">minimal</font></b> output.<br
align="left"/>
-  Intermediate results stored in <i><font color="forestgreen">objects</font>
</i> for further processing.<br align="left"/>
>]

packages [label=<<i><font color="forestgreen">Packages</font></i>:<br
align="left"/>
- Enable additional/advanced tools.<br align="left"/>
- About 25 "standard"/"recommended" packages.<br align="left"/>
- Search <i><font color="forestgreen">CRAN</font></i> Internet sites for more
 specific/advanced statistics tools.<br align="left"/>
>]

interactive_session [label=<Interactive <i><font color="forestgreen">sessions
</font></i> (<b><font color="red">ephemeral</font></b> program):<br
align="left"/>
1. Create a working directory and change to it.<br align="left"/>
2. Start a sessions with <b><font color="blue" face="Source Code Pro">
R</font></b>.<br align="left"/>
3. Issue commands...<br align="left"/>
4. Quit with command <b><font color="blue" face="Source Code Pro">q()</font>
</b>.<br align="left"/>
5. Choose <b><font color="blue" face="Source Code Pro">y</font></b> to save
 data of current session (<b><font color="red">optional</font></b>)<sup><b><font
color="red">1</font></b></sup>.<br align="left"/>
>]

promt [label=<<i><font color="forestgreen">Promt</font></i>:<br align="left"/>
- Waiting for command input.<br align="left"/>
- Default symbol is <b><font color="blue" face="Source Code Pro">&gt;</font>
</b>.<br align="left"/>
- Change to <b><font color="blue" face="Source Code Pro">+</font></b> when a
 command is split on multiple lines.<br align="left"/>
>]

help [label=<Getting <i><font color="forestgreen">help</font></i>:<br
align="left"/>
- <b><font color="blue" face="Source Code Pro">help()</font></b> or <b><font
color="blue" face="Source Code Pro">?</font></b>: information on named function.
<br align="left"/>
- <b><font color="blue" face="Source Code Pro">help.search()</font></b> or
 <b><font color="blue" face="Source Code Pro">??</font></b>: search a term.<br
align="left"/>
- <i><font color="forestgreen">Operators</font></i> and <i><font
color="forestgreen">keywords</font></i> needs double <b><font color="blue"
face="Source Code Pro">&quot;</font></b> or <b><font color="blue"
face="Source Code Pro">&#39;</font></b> single quotes.<br align="left"/>
- <b><font color="blue" face="Source Code Pro">help.start()</font></b>: launch
 web browser with help in HTML format.<br align="left"/>
>]

help_ex [label=<Example show info:<br align="left"/>
<font face="Source Code Pro">
&gt; # Shows info for solve function<br align="left"/>
&gt; <b><font color="blue">help(solve)</font></b><br align="left"/>
&gt; # Shows info for [[ operator<br align="left"/>
&gt; <b><font color="blue">?&quot;[[&quot;</font></b>
<br align="left"/>
</font>
>]

help_ex_search [label=<Example search:<br align="left"/>
<font face="Source Code Pro">
&gt; # Search while keyword<br align="left"/>
&gt; <b><font color="blue">help.search(&#39;while&#39;)</font></b><br
align="left"/>
&gt; # Search read term<br align="left"/>
&gt; <b><font color="blue">??read</font></b>
<br align="left"/>
</font>
>]

history [label=<Command <i><font color="forestgreen">history</font></i>:<br
align="left"/>
- Use vertical arrow keys to scroll last commands.<br align="left"/>
- Then enter key to repeat selected command.<br align="left"/>
- <sup><b><font color="red">1</font></b></sup>Can be saved to <b><font
color="blue" face="Source Code Pro">.Rhistory</font></b> file (<b><font
color="red">optional</font></b>).<br align="left"/>
>]

source_file [label=<<b><font color="red">Persistent</font></b> program:<br
align="left"/>
- <i><font color="forestgreen">Script</font></i>:<br align="left"/>
 + Save sequence of commands to a <b><font color="blue"
face="Source Code Pro">.R</font></b> file.<br align="left"/>
 + <b><font color="blue" face="Source Code Pro">source(&quot;</font></b>
file_path<b><font color="blue" face="Source Code Pro">.R&quot;)</font></b>:
 execute a script.<br align="left"/>
- Session log:<br align="left"/>
 + <b><font color="blue" face="Source Code Pro">sink(&quot;</font></b>
file_path<b><font color="blue" face="Source Code Pro">.lis&quot;)</font></b>:
 save console's output to file.<br align="left"/>
 + <b><font color="blue" face="Source Code Pro">sink()</font></b>: reset output
 to console.<br align="left"/>
>]

valid_names [label=<Valid <i><font color="forestgreen">names</font></i>:<br
align="left"/>
- Can contain combination of symbols:<br align="left"/>
 + alphabetic (<i><font color="forestgreen">ASCII</font></i> <b><font
color="red">only</font></b>)<br align="left"/>
 + numeric <br align="left"/>
 + dot <b><font color="blue" face="Source Code Pro">.</font></b><br
 align="left"/>
 + underscore <b><font color="blue" face="Source Code Pro">_</font></b><br
 align="left"/>
- If first symbol is a dot <b><font color="blue" face="Source Code Pro">.</font>
</b>, next <b><font color="red">must not</font></b> be a digit.<br
align="left"/>
- No limit on length.<br align="left"/>
- <i><font color="forestgreen">Case sensitive:</font></i> <b><font color="blue"
face="Source Code Pro">var</font></b> is different to <b><font color="blue"
face="Source Code Pro">VAR</font></b>.<br align="left"/>
>]

compound_exp [label=<<i><font color="forestgreen">Compound expressions</font>
</i>:<br align="left"/>
- Between braces <b><font color="blue" face="Source Code Pro">{}</font></b>:<br
align="left"/>
 + A series of <i><font color="forestgreen">elementary commands</font></i>
<br align="left"/>
>]

elementary_commands [label=<<i><font color="forestgreen">Elementary commands
</font></i>:<br align="left"/>
>]

expressions [label=<<i><font color="forestgreen">Expressions</font></i> (core
 of <b><font color="blue"
face="Source Code Pro">R</font></b>):<br align="left"/>
- evaluated<br align="left"/>
- printed (by default)<br align="left"/>
- result value is <b><font color="red">lost</font></b><br align="left"/>
>]

assignments [label=<<i><font color="forestgreen">Assignements</font></i>:<br
align="left"/>
- evaluates an <i><font color="forestgreen">expression</font></i><br
align="left"/>
- bind value to variable<br align="left"/>
- no printed (by default)<br align="left"/>
>]

separation [label=<Separated by:<br align="left"/>
- Semi-colon <b><font color="blue" face="Source Code Pro">;</font></b>.<br
align="left"/>
- New line (<b><font color="red">limit</font></b> of <font
face="Source Code Pro">4095</font> bytes by line).<br align="left"/>
>]

comments [label=<<i><font color="forestgreen">Comments</font></i>:<br
align="left"/>
- Begins with hash mark <b><font color="blue" face="Source Code Pro">#</font>
</b> until end of line.<br align="left"/>
- <b><font color="red">Doesn't</font></b> work:<br align="left"/>
 + inside strings <br align="left"/>
 + argument list of function definition.<br align="left"/>
>]

objects [label=<<i><font color="forestgreen">Objects</font></i>:<br
align="left"/>
- Entities that <b><font color="blue" face="Source Code Pro">R</font></b>
 creates and manipulates.<br align="left"/>
- Are create and stored by <i><font color="forestgreen">name</font></i>.<br
align="left"/>
- <b><font color="blue" face="Source Code Pro">objects()</font></b> or <b><font
color="blue" face="Source Code Pro">ls()</font></b>: list objects on <i><font
color="forestgreen">workspace</font></i>.<br align="left"/>
- <b><font color="blue" face="Source Code Pro">rm(name1,...,nameN)</font></b>:
 removes objects by <i><font color="forestgreen">name</font></i> from <i><font
color="forestgreen">workspace</font></i>.<br align="left"/>
>]

objects_ex [label=<Examples:<br align="left"/>
- variables<br align="left"/>
- arrays of numbers<br align="left"/>
- character strings<br align="left"/>
- functions<br align="left"/>
- structures built from combinations of abode.<br align="left"/>
>]

workspace [label=<<i><font color="forestgreen">Workspace</font></i>:<br
align="left"/>
- Current collection of objects on a session.<br align="left"/>
- <sup><b><font color="red">1</font></b></sup>Can be saved to <b><font
color="blue" face="Source Code Pro">.RData</font></b> file (<b><font
color="red">optional</font></b>).<br align="left"/>
- <b><font color="red">Avoid</font></b> namespace collisions isolating
 projects with separated folders.<br align="left"/>
>]

// Connections
r_enviro -> s_lang
s_lang -> s_philosophy
r_enviro -> packages
r_enviro -> interactive_session
interactive_session -> promt
interactive_session -> help
interactive_session -> history
interactive_session -> source_file
help -> help_ex
help -> help_ex_search
r_enviro -> valid_names
r_enviro -> compound_exp
compound_exp -> elementary_commands
elementary_commands -> expressions
elementary_commands -> assignments
elementary_commands -> separation
r_enviro -> comments
r_enviro -> objects
objects -> objects_ex
objects -> workspace
}
