# Description

Highlights and _"mnemonic"_ graphs for:
<https://cran.r-project.org/doc/manuals/r-release/R-intro.html>

# Installation

On Arch Linux:

```bash
sudo pacman -S r openblas
```

> **Note:** needs a capital `R` to launch a session.
