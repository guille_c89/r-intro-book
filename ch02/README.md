# 2 Simple manipulations; numbers and vectors

[[_TOC_]]

## 2.1 Vectors and assignment

- `R` operates on _data structures_.
- Simplest structure is _numeric vector_.
- _Numeric vector_: ordered collection of numbers.
- `c(v1,...,vN)`: concatenate input vectors.
- A number (in a expression) is a **vector of length one**.
- `<-`: assignment operator, _"points"_ right value to left object.

```R
x <- c(10.4, 5.6, 3.1, 6.4, 21.7)
```

> **Note:** `=` is alternative to `<-` on most cases.

- `assign(`_obj_`,` _expression_`)`: larger version of `<-`.

```R
assign("x", c(10.4, 5.6, 3.1, 6.4, 21.7))
```

- `->`: "reverse" direction assignation operator.

```R
c(10.4, 5.6, 3.1, 6.4, 21.7) -> x
```

- A expression as command prints value and **loss** it.

## 2.2 Vector arithmetic

- Arithmetic operations on vectors are performed element by element.
- Vector on a expressions **not** need to be of same length.
- Shorter vectors on a expression are _recycled_.
- _Recycled_: (partially) repeated until match longest vector on expression.
  + Example: a constant is repeated `n` times, `n` is the vector's length.

```R
> # Previous values.
> # x length: 5
> x <- c(10.4, 5.6, 3.1, 6.4, 21.7)
> x
 [1] 10.4  5.6  3.1  6.4 21.7
> # y length: 11
> y <- c(x, 0, x)
> y
 [1] 10.4  5.6  3.1  6.4 21.7  0.0 10.4  5.6  3.1  6.4 21.7
>
> # x vector is recycled to match y length
> # This produce a warning.
> v <- 2*x + y + 1
Warning message:
In 2 * x + y :
  longer object length is not a multiple of shorter object length
> # v has same length of the longest vector on expression (y).
> v
 [1] 32.2 17.8 10.3 20.2 66.1 21.8 22.6 12.8 16.9 50.8 43.5
```

Simple vector for next examples:

```R
> z <- c(5, 4, 3, 2, 1)
> z
[1] 5 4 3 2 1
```

- Elementary arithmetic operators:
  + `+`: sum
  + `-`: rest
  + `*`: multiplication
  + `/`: division
  + `^`: raising to a power

  ```R
  > ## Sum (element by element)
  > z+2
  [1] 7 6 5 4 3
  > ## Rest (element by element)
  > z-2
  [1]  3  2  1  0 -1
  > ## Multiplication (element by element)
  > z*2
  [1] 10  8  6  4  2
  > ## Division (element by element)
  > z/2
  [1] 2.5 2.0 1.5 1.0 0.5
  > ## Power (element by element)
  > z^2
  [1] 25 16  9  4  1
  ```

- Common arithmetic functions:
  + `log()`: natural logarithm
  + `exp()`: natural exponent
  + `sqrt()`: square root

  ```R
  > # Logarithm (natural)
  > log(z)
  [1] 1.6094379 1.3862944 1.0986123 0.6931472 0.0000000
  > # Exponent (natural)
  > exp(z)
  [1] 148.413159  54.598150  20.085537   7.389056   2.718282
  > # Square root
  > sqrt(z)
  [1] 2.236068 2.000000 1.732051 1.414214 1.000000
  ```

  > **Note:** "usual meaning", use `?` (help) when doubt.

- Trigonometric functions:
  + `sin()`: sine
  + `cos()`: cosine
  + `tan()`: tangent

   ```R
   > # Trigonometric Functions
   > ## Sine
   > sin(z)
   [1] -0.9589243 -0.7568025  0.1411200  0.9092974  0.8414710
   > ## Cosine
   > cos(z)
   [1]  0.2836622 -0.6536436 -0.9899925 -0.4161468  0.5403023
   > ## Tangent
   > tan(z)
   [1] -3.3805150  1.1578213 -0.1425465 -2.1850399  1.5574077
   ```

- Common functions over vectors:
  + `max()`: return largest element.
  + `min()`: return smallest element.
  + `range()`: equivalent to `c(min(x), max(x))`.
  + `length()`: number of elements.
  + `sum()`: sum of all elements.
  + `prod()`: product of elements.

  ```R
  > # Maximum and minimum of a vector
  > max(z)
  [1] 5
  > min(z)
  [1] 1
  > # Range, previous values on a vector
  > range(z)
  [1] 1 5
  > # Equivalent to:
  > c(min(z), max(z))
  [1] 1 5
  >
  > # Length of a vector
  > length(z)
  [1] 5
  > # A number is a vector of length 1
  > length(8)
  [1] 1
  >
  > # Summation of elements of a vector
  > sum(z)
  [1] 15
  > # Product of elements of a vector
  > prod(z)
  [1] 120
  ```

- Basic statistical functions:
  + `mean()`: sample mean, equivalent to `sum(x)/length(x)`.
  + `var()`: sample variance, equivalent to `sum((x-mean(x))^2)/(length(x)-1)`.

  ```R
  > # Mean of a vector
  > mean(z)
  [1] 3
  > # Equivalent to:
  > sum(z)/length(z)
  [1] 3
  >
  > # Variance of a vector
  > var(z)
  [1] 2.5
  > # Equivalent to:
  > sum((z-mean(z))^2)/(length(z)-1)
  [1] 2.5
  ```

- `sort()`: return a vector with elements arranged in increasing order.

  ```R
  > # Sort
  > sort(z)
  [1] 1 2 3 4 5
  ```

- `pmax()`/`pmin()`: compare by element on same position (parallel).

  ```R
  > # Parallel maximum
  > pmax(z, sort(z))
  [1] 5 4 3 4 5
  > # Parallel minimum
  > pmin(z, sort(z))
  [1] 1 2 3 2 1
  ```

- All internal calculations use **double** float numbers.
- Complex numbers need **explicit** complex part. Example: `sqrt(-17+0i)`.

  ```R
  > # Imaginary numbers needs explicit i
  > sqrt(-17) # NaN error
  [1] NaN
  Warning message:
  In sqrt(-17) : NaNs produced
  > sqrt(-17+0i)
  [1] 0+4.123106i
  ```

## 2.3 Generating regular sequences

Facilities for generate sequences of numbers: `:`, `seq()` and `rep()`.

- `:` operator.
  + `1:10` generates a vector with sequence from `1`:`10`.
  + `10:1` sequence backwards.

    ```R
    > # : operator
    > # Generates a numeric vector from 1 to 10
    > 1:10
     [1]  1  2  3  4  5  6  7  8  9 10
    > # Backwards
    > 10:1
     [1] 10  9  8  7  6  5  4  3  2  1
    ```

  + Higher priority than arithmetic operators: `1:10-1` equivalent to
    `(1:10)-1`.

    ```R
    > # : operator has higher priority than arithmetic operators
    > 1:10-1
     [1] 0 1 2 3 4 5 6 7 8 9
    > # Equivalent to:
    > (1:10)-1
     [1] 0 1 2 3 4 5 6 7 8 9
    > # Note: use () when needed
    > 1:(10-1)
    [1] 1 2 3 4 5 6 7 8 9
    ```

- `seq()` function: more general than `:` operator.
  + Has five arguments:
    * `from=` sequence's begin.
    * `to=` sequence's end.
    * `by=` step size.
    * `length=` number of equally distant elements on sequence.
    * `along=` generate sequence from `1` to `length()` of a vector.
  + If `by` and `lenght` are **omitted**, step size defaults to `1`.
  + `by` and `length` are mutually exclusive.
  + `along` is used as solo argument.

  ```R
  > # seq() a more general sequence generator
  > # Equivalent to 1:10
  > seq(from=1, to=10)
   [1]  1  2  3  4  5  6  7  8  9 10
  > # Backwards
  > seq(from=10, to=1)
   [1] 10  9  8  7  6  5  4  3  2  1
  > # Setting step size to 3
  > seq(from=1, to=10, by= 3)
  [1]  1  4  7 10
  > # Setting number of equally distant elements to 3
  > seq(from=1, to=10, length= 3)
  [1]  1.0  5.5 10.0
  > # Sequence from another vector
  > x <- seq(from=100, to=95)
  > x
  [1] 100  99  98  97  96  95
  > seq(along=x)
  [1] 1 2 3 4 5 6
  ```

- `rep()` function: replicate an object.
  + Nameless argument: an object (like numeric vector).
  + `times=` copies end-to-end.
  + `each=` repeats each element before next.

  ```R
  > # Vector for examples
  > y <- 1:3
  > y
  [1] 1 2 3
  > # rep() replicate objects
  > # end-to-end 2 times
  > rep(y, times=2)
  [1] 1 2 3 1 2 3
  > # 2 times each element
  > rep(y, each=2)
  [1] 1 1 2 2 3 3
  ```

- Arguments in named form: `name=value`.
- Named arguments order are **irrelevant**.

  ```R
  > # Named argument's irrelevant order
  > rep(times=3, seq(to=9, from=7))
  [1] 7 8 9 7 8 9 7 8 9
  ```

- Named arguments **preferred** on scripts.

## 2.4 Logical Vectors

- Elements have only `3` values:
  + `TRUE`
  + `FALSE`
  + `NA`: not available
- `TRUE` and `FALSE` can be abbreviated to `T` and `F`.
- `T` and `F` are not _keywords_, so they can be **overwritten**.
- **Avoid** use of `T` and `F` abbreviations.
- _Conditions_: generates logical vectors.

```R
> # Simple numeric vector for examples
> x <- 1:3
> x
[1] 1 2 3
> # Logical vectors are generate from conditions
> x > 0
[1] TRUE TRUE TRUE
```

- _Logical operators_ (comparison between numbers) are:
  + `<`
  + `<=`
  + `>`
  + `>=`
  + `==`: exact equality
  + `!=`: inequality

```R
> # Logical operators
> ## Numeric comparisons:
> ### Less than
> x < 2
[1]  TRUE FALSE FALSE
> ### Less or equal to
> x <= 2
[1]  TRUE  TRUE FALSE
> ### Greater than
> x > 2
[1] FALSE FALSE  TRUE
> ### Greater or equal to
> x >= 2
[1] FALSE  TRUE  TRUE
> ### Equal to
> x == 2
[1] FALSE  TRUE FALSE
> ### Not equal to
> x != 2
[1]  TRUE FALSE  TRUE
```

- Operators between logical quantities:
  + `&`: intersection/_"and"_
  + `|`: union/_"or"_
  + `!`: negation (unitary operator)

```R
> ## Logical comparisons:
> ### Base vectors for examples
> y <- x == 2
> y
[1] FALSE  TRUE FALSE
> z <- x != 2
> z
[1]  TRUE FALSE  TRUE
> ### Intersection/"and"
> y & z
[1] FALSE FALSE FALSE
> ### Union/"or"
> y | z
[1] TRUE TRUE TRUE
> ### Negation (unitary operator)
> !y
[1]  TRUE FALSE  TRUE
```

- Logical vectors can be used in arithmetic expressions.
- Coerced numeric values:
  + `FALSE` to `0`.
  + `TRUE` to `1`.
- Some values (like `NA`) hasn't numeric equivalent.

## 2.5 Missing values

- `NA`: elements not completely known:
  + _"not available"_
  + _"missing value"_
- Anything involving `NA` becomes `NA`.
- `is.na()`: check by element if value is `NA` or `NaN`.
- `NaN`: _Not a Number_.
- Result of expressions that cannot be defined, like:
  + `0/0`
  + `Inf - Inf`
- `Inf`: infitine.
- `is.nan()`: only `TRUE` for `NaN`s.
- `<NA>`: variation when a _character vector_ is printed without quotes.
